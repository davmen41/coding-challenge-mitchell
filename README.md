# Coding Challenge - Mitchell International

This coding challenge was implemented using Java and JUnit for automated testing. It uses a HashMap to store the vehicle objects 
and provide quicker access time for deletions and updates. The data is persisted in memory by using serialization, the HashMap is
stored inside a text file and is read from and updated depending on the operation. This method was chosen for its portability, allowing me
to share the code with others without any major issues. 

# Getting Started.
This project was done using Eclipse as my IDE and Java along with JUnit for automated testing. Once the repository is downloaded it can be opened
and ran.

# Running the tests
Once the project is built, running main (located in VehiclesTestMain) will run all automated tests and display 
any failures or if all tests ran successfully.

#Built With
Eclipse
Java - Source Code
JUnit - Automated Testing

# Author
David Mendoza